﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Breaout_Final_Try.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Breaout_Final_Try.Gameplay
{
    public class GamePlayScreen : GameScreen
    {

        SpriteBatch batch;
        SpriteFont font;
        Texture2D image;
        GamePlayManager manager;
        GameLevelManager levelManager;
        Game game;

        public GamePlayScreen(Game game,SpriteBatch batch) : base(game)
        {

            this.batch = batch;
            
            this.game = game;
            manager = new GamePlayManager(game,batch);

            // the 0.24f is the calculation I got from this site http://www.gamedev.net/topic/679266-scale-entity-dynamically/
            /*********
             * Basically, Since i want the blocks to fill the 1/4 of the screen I divide the width of the screen by. Then the result is divided by the how many bricks I want in a row(8)
             * and then divided it by the image size of the texture. I use photoshop to get its image size and use it to be divided by the result of the 2nd calculation.
             * 
             * *********/
            levelManager = new GameLevelManager(this.game, this.batch,manager.getBall,manager.getPaddle, scale: 0.24f);
       
            GameComponents.Add(levelManager);
            GameComponents.Add(manager);    // Dont forget to always add it to the GameComponent
        }

        protected override void LoadContent()
        {
            image = game.Content.Load<Texture2D>("GamePlayBG");
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            // render the background. Well let the GamePlayScreen Do this to avoid confusion
            batch.Draw(image,Vector2.Zero,Color.White); 
            base.Draw(gameTime);
        }
    }
}
