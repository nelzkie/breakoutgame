﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Breaout_Final_Try.Gameplay
{
    public class Camera2D
    {
        protected float _zoom; // Camera Zoom
        public Matrix _transform; // Matrix Transform
        public Vector2 _pos; // Camera Position
        protected float _rotation; // Camera Rotation
        private float shaketime = 0.2f;
        public static bool shake { get; set; }

        public Camera2D()
        {
            _zoom = 1.0f;
            _rotation = 0.0f;
            _pos = Vector2.Zero;
        }

        // Sets and gets zoom
        public float Zoom
        {
            get { return _zoom; }
            set { _zoom = value; if (_zoom < 0.1f) _zoom = 0.1f; } // Negative zoom will flip image, so clamp it to just 0.1 as minimum value
        }

        public float Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }

        // Auxiliary function to move the camera
        public void Move(Vector2 amount)
        {
            _pos += amount;
        }
        // Get set position
        public Vector2 Pos
        {
            get { return _pos; }
            set { _pos = value; }
        }


        public void Update(GameTime time)
        {
            float dt = (float)time.TotalGameTime.TotalSeconds;  // Note: here we use TotalGameTime not ElapseGametime. Look it up on google to see difference


            int speedpulsate = 60; // the speed of pulsate

            //Basically the Math.Cos and/or Math.Sin will return a value between 1 and -1;
            // Never forget to experiment to understand it clearly.



            if (shake)
            {
                float pulsateX = (float)Math.Cos(dt * speedpulsate) * 0.6f;
                float pulsateY = (float)Math.Sin(dt * speedpulsate) * 0.3f;
                shaketime -= 1.0f * (float)time.ElapsedGameTime.TotalSeconds;
                Move(new Vector2(pulsateX, _pos.Y));
            }
            if (shaketime <= 0.0f)
            {
                shake = false;
                _pos = Vector2.Zero;
                shaketime = 0.2f;
            }


        }
        public Matrix get_transformation(GraphicsDevice graphicsDevice)
        {
            _transform =       // Thanks to o KB o for this solution
              Matrix.CreateTranslation(new Vector3(_pos.X, _pos.Y, 0)) *
                                         Matrix.CreateRotationZ(Rotation) *
                                         Matrix.CreateScale(new Vector3(Zoom, Zoom, 1)) *
                                         Matrix.CreateTranslation(new Vector3(0, 0, 0));
            return _transform;
        }

    }
}

