﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Breaout_Final_Try.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Shapes;

namespace Breaout_Final_Try.Gameplay.Entities
{


   public class PowerUps : GameEntity
    {
        public SpriteBatch batch { get; set; }
        public bool Activated { get; set; }
        public int Index { get; set; }
        public float TTL { get; set; } // Time to live
        public bool lengthen;
        public  Color color { get; set; }
        Vector2 scale;
        int timeCounter;
        float timer;
        Texture2D image;
        Paddle paddle;
        private  bool paddlehit;
        GameTime currentTime;
        public Dictionary<PowerUpType, int> PowerUpDictionary;
        PowerUpType type;
        public PowerUps(Game game, Texture2D image, SpriteBatch batch, Vector2 position,Paddle paddle, Vector2 scale, float speed, int index) : base(game, image, batch, position, scale, speed)
       {
            PowerUpDictionary = new Dictionary<PowerUpType, int>();
            

            Direction = new Vector2(0,1);
            Index = index;

            if(index == 0)
            {
                color = Color.White;
            }else if(index == 1)
            {
                type = PowerUpType.Lengthen;
                color = Color.Green;
            }else if(index == 2)
            {
                color = Color.Red;
            }else if(index == 3)
            {
                color = Color.Orange;
            }
            this.batch = batch;
            this.scale = scale;
            this.image = image;
            this.paddle = paddle;

            TTL = 20;
            PowerUpDictionary.Add(type,10);
            
            Debug.WriteLine("width power up" + width + "   " + scale.X);
        }



        public bool PowerUpPaddleCollision(Paddle paddle)
        {
            if (paddle.bounds.Intersects(this.bounds))
            {
                paddlehit = true;
                paddle.color = color;
                return true;
            }
            
           
            return false;
        }

        public override void update(GameTime time)
       {
            bounds = new Rectangle((int)position.X, (int)position.Y, (int)width * (int)Scale.X,(int)height * (int)Scale.Y);

            if (Activated)
            {
                
                position += ((Direction * (float)time.ElapsedGameTime.TotalSeconds) * speed);
                base.update(time);
            }

        }



       public override void render(GameTime time)
       {
            if (Activated)
            {
                batch.DrawRectangle(bounds,Color.Red,2f);
                batch.Draw(image, position, null, color, 0f, Vector2.Zero, scale, SpriteEffects.None, 0);
            }
       }
    }
}
