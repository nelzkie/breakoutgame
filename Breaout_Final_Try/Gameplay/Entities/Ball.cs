﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Breaout_Final_Try.Managers;
using Breaout_Final_Try.Sound;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Shapes;

namespace Breaout_Final_Try.Gameplay.Entities
{
    public class Ball : GameEntity
    {
        public int life { get; set; }
        public bool BrickIsBreakable { get; set; }
        private Paddle paddle;
        KeyboardState oldstate, currentState;
        public bool ResetBall { get; set; }
        public bool ReleaseBall { get; set; }
        public bool DebugActive { get; set; }
        private bool justStater = true;

        public bool levelAnimationDone { get; set; }
        float radius;

        private bool occupied;
        private Texture2D dotDebug;

        public GameLevelManager LevelManager;
        private bool stuck { get; set; }
        float offset = 0;
        SpriteFont lifeFont;
        SoundHelper soundHelper;
        
        
        public Ball(Game game, Texture2D image, SpriteBatch batch, Vector2 position, float scale, float speed) 
            : base(game, image, batch, position, scale, speed)
        {
            ReleaseBall = false;
            ResetBall = true;
            radius = height / 2;
        }
        public Ball(Game game, Texture2D image, SpriteBatch batch, Vector2 position, Paddle paddle,float scale, float speed)
     : base(game, image, batch, position, scale, speed)
        {
            this.paddle = paddle;
            ReleaseBall = false;
            ResetBall = true;
            radius = height / 2;
            soundHelper = new SoundHelper(game.Content);
            Reset_Ball();
            Direction = new Vector2(0, -1);
            dotDebug = game.Content.Load<Texture2D>("dot");
         
            life = 2;
            Vector2 clampsample = Vector2.Clamp(new Vector2(100, -10), new Vector2(20, 10), new Vector2(10, -20));
            Debug.WriteLine(clampsample);

        }

        public override void update(GameTime time)
        {
            occupied = false;
            currentState = Keyboard.GetState();
            
            if (CheckPressOnce(Keys.Space) && levelAnimationDone)
            {
                ReleaseBall = true; // trigger to release the ball
                stuck = false;
        
                //paddle.buttonPress = stuck;
                paddle.sticky = stuck;
            }

            if (ReleaseBall && ResetBall)
            {
                Debug.WriteLine("haha");
                
                ReleaseBall = false;
                ResetBall = false;
                ActivateCollision();
            }
            if (justStater)
            {
                Reset_Ball(); // Reset the ball
            }

            ViewportCollision();

            if (Circle_ABB_Collision(paddle) && !stuck)
            {
                // A hack since the ball always return true we up the ball by 3 units and then deflect it to avoid the double bounce problem
                position = new Vector2(position.X, paddle.position.Y - height);
                deflectBall(paddle);
            }

        
            if (!stuck)
            {
                
                position += ((Direction * (float)time.ElapsedGameTime.TotalSeconds) * speed);
            }
            else
            {
                if (!ResetBall)
                {
                    move(time);
                }
            }


            base.update(time);

            oldstate = currentState;
        }

        public override void render(GameTime time)
        {

           
            base.render(time);
            if (DebugActive)
            {
                Batch.DrawCircle(getCenter(), radius, 50, Color.BlueViolet, 2f);
                Batch.Draw(dotDebug, ClosestDebug, Color.Yellow);
                Batch.Draw(dotDebug, getCenter(), Color.Cyan);
                Batch.Draw(dotDebug, paddle.position + differentDebug2, Color.Red);
                Batch.Draw(dotDebug, paddle.position + clampedDebug, Color.Green);
                Batch.DrawRectangle(paddle.bounds,Color.Red,1f);

            }

        }
        private bool CheckPressOnce(Keys key)
        {
            return currentState.IsKeyUp(key) && oldstate.IsKeyDown(key);
        }

        /// <summary>
        /// Check the collision between the screen and the ball
        /// </summary>
        private void ViewportCollision()
        {
            //JustStart(); // set the justStart to false so that the other function will work

            
            if (position.Y < 0)
            {
                soundHelper.Bounce().Play();
                Direction = new Vector2(Direction.X, -Direction.Y);
            }

            if (position.X < 0 || position.X + width > viewrportBounds.Width)
            {
                soundHelper.Bounce().Play();
                Direction = new Vector2(-Direction.X, Direction.Y);
            }

            if (position.Y + height > viewrportBounds.Height)
            {
                Reset_Ball();
                life--;
                if(life < 0)
                {
                    life = 0;
                }
            }
        }

        private Vector2 getCenter()
        {
            return new Vector2(this.position.X + width / 2, this.position.Y + height / 2);
        }

        Vector2 AABB_Half_extents_Debug;
        Vector2 AABB_Center_debug;
        Vector2 clampedDebug;
        Vector2 ClosestDebug;
        Vector2 differentDebug, differentDebug2;

        /// <summary>
        /// Circle-Rectangle collision. Can be use in paddle and in blocks
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Circle_ABB_Collision(GameEntity entity)
        {
        
       
            Vector2 center = getCenter();
            float rr = getCenter().X;
            Vector2 AABB_Half_Extents = new Vector2(entity.width / 2, entity.height / 2);
            AABB_Half_extents_Debug = AABB_Half_Extents; // For debugging purposes only

            Vector2 AABB_Center = new Vector2(entity.position.X + AABB_Half_Extents.X, entity.position.Y + AABB_Half_Extents.Y); // this is the center of the rectangle
            AABB_Center_debug = AABB_Center;  // For debugging purposes only


            // We first need to get the difference vector between ball's center and the paddle's center
            Vector2 difference = center - AABB_Center;

            differentDebug = difference;
           
            // Then clamp this difference vector to the paddle's half extents width and height
            Vector2 clamped = Vector2.Clamp(difference, -AABB_Half_Extents, AABB_Half_Extents);
            clampedDebug = clamped;
           

            // This returns a vector that is always somewhere located at the edge of the paddle
            // Add clamped value to AABB_center and we get the value of box closest to circle
            Vector2 closest = AABB_Center + clamped;
            ClosestDebug = closest;
         

            // Never think of this as a point or direction, its just a numerical value to compare to the ball's radius
            difference = closest - center;
            differentDebug2 = difference;
          
            // we have to round up the lenght to 2 digits
            if (Math.Round(difference.Length(),2) < radius)
            {
               if(entity is Paddle)
               {
                    
                    offset = position.X - paddle.position.X;
                }
                return true;
            }
            return false;

        }

        /// <summary>
        /// Position the ball to the top of the paddle
        /// </summary>
        public void Reset_Ball()
        {

            /****************
             * Since we set a center on our game entity class rendere, we dont have to get the paddle.withe / 2 and ball.with / 2 because the ball will
             * automatically position on center
             * 
             * ****************/

            float x = paddle.position.X + paddle.width / 2 - width / 2;
       
            //float x = paddle.position.X;
            float y = paddle.position.Y - height ;
            position = new Vector2(x,y);

            offset = position.X - paddle.position.X;
            paddle.sticky = true;
            justStater = true;
            stuck = true;
            ResetBall = true;
           

        }
    
        private void move(GameTime time)
        {

            // here we position the ball relative to paddle position and retain the ball to the position on where it hits the paddle no matter where the paddl goes
            if(paddle.paddleState == PaddleState.Moving)
            {
                float x = paddle.position.X + offset;
                position = new Vector2(x, position.Y);
              
            }
        }

        /// <summary>
        /// Trigger to activate the collision function in the update fuction
        /// </summary>
        private void ActivateCollision()
        {
     
                justStater = false; // activate the collision flag
            
        }


        /// <summary>
        /// Deflect the ball base on where it hits the paddle or the block
        /// </summary>
        /// <param name="entity"></param>
        public void deflectBall(GameEntity entity)
        {
            if (!occupied )
            {
               
                float center;
                center = entity.position.X + entity.width / 2; // get the center of the paddle
                float distance = (this.position.X + this.radius) - center; // get the distance of the ball from the paddle's center.
                float percentage = distance / (entity.width / 2); // make a percentage of how much angle will the ball bounce base on how far it landed from the paddle's center
                                                                  // the percentage will give -1 to 1 value base on where it hit the paddle

                /**************
                 *  This is a hack. Since the ball will be always hitting the paddle everytime it stays idle and the hasnt been fired yet, the hitPaddle() will always return true.
                 *  If we set the speed to 100 you will a glitch if this hack is not place. So to fix that, we up the position.Y by 1 pixel so that it wont touch it again the first time it touch the
                 *  paddle
                 * 
                 * **************/

             //this.position = new Vector2(this.position.X, this.position.Y - 2f);
                
                // this.Speed = 0f;

                this.Direction = new Vector2(percentage * 0.5f, -this.Direction.Y);
                //this.Direction = new Vector2(this.Direction.X, -this.Direction.Y);
                //this.Direction.Normalize();
                occupied = true;
                if (entity is Paddle)
                {

                    stuck = paddle.sticky;

                }

            }
        }




    }
}
