﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Shapes;

namespace Breaout_Final_Try.Gameplay.Entities
{
    public enum Powers
    {
        lengthen,
        sticky,
        pass_through,
        none
    }
    public enum PaddleState
    {
        Moving,
        Idle
    }

    public class Paddle : GameEntity
    {

        float timer;
        int timeCounter;
        public Dictionary<Powers, int> powers = new Dictionary<Powers, int>();
        private Ball ball;
        float paddleLenghten;
        float originallenght;
        public bool buttonPress { get; set; }
        public bool sticky { get; set; }
        public bool pass_through { get; set; }
        public PaddleState paddleState;
        public bool ballStuck { get; set; }
        public Paddle(Game game,Texture2D image, SpriteBatch batch, Vector2 position, float scale, float speed) : base(game,image, batch, position, scale, speed)
        {
            powers.Add(Powers.lengthen, 0);
            powers.Add(Powers.sticky, 0);
            powers.Add(Powers.pass_through,0);
            paddleLenghten = width + 20;
            originallenght = width;
            sticky = true;
            pass_through = false;
            paddleState = PaddleState.Idle;
        }


        public void pickPowerUp(Powers type, int duration)
        {
            //powers[type] = 0;
            powers[type] = timeCounter + duration;
        }
        public override void update(GameTime time)
        {
            KeyboardState state = Keyboard.GetState();
            paddleState = PaddleState.Idle; // set the paddlestate to Idle
            Inpulthandler(state);
            
            position = ViewportCollision(time);



            foreach (var power in powers)
            {
                // WE need to update the timer if we found power duration greater than the pervious timeCounter
                // Reference : http://www.gamedev.net/topic/679658-powerup-stuck/
                if (power.Value > timeCounter && power.Key != Powers.none)
                {
                    timer += (float)time.ElapsedGameTime.TotalSeconds;
                    timeCounter += (int)timer;
                    if (timer >= 1.0F) timer = 0F;
                }
                else
                {
                    color = Color.White;
                }
            }
            width = getPaddleSize();
            sticky = PowerUpStickyEffect();
            pass_through = PowerUpPassthrough();
        
            base.update(time);
        }

        private bool powerIsActive(Powers type)
        {
         
            return powers[type] > timeCounter;
        }

        private bool PowerUpStickyEffect()
        {
            if (powerIsActive(Powers.sticky))
                return true;
           
            return false;
        }
        private bool PowerUpPassthrough()
        {
            if (powerIsActive(Powers.pass_through))
            {
               
                return true;
            }
            
            return false;
        }
        private float getPaddleSize()
        {
            if(powerIsActive(Powers.lengthen))
            {
                
                return paddleLenghten;
            }
            return originallenght;
        }

        private void resetPower(Powers type)
        {
            powers[type] = 0;
        }

        public void reset()
        {
            //timeCounter = 0;
            //foreach (var power in powers)
            //{
            //    resetPower(power.Key);
            //}
            
            
        }
        public override void render(GameTime time)
        {
            
            base.render(time);
        }

        private void Inpulthandler(KeyboardState state)
        {
            Vector2 tmpDirection = Vector2.Zero; 

            Dictionary<Keys,Vector2> KeyMap = new Dictionary<Keys, Vector2>();
            KeyMap.Add(Keys.Left,new Vector2(-1,0));
            KeyMap.Add(Keys.Right,new Vector2(1,0));

            foreach (var key in KeyMap)
            {

                if (state.IsKeyDown(key.Key))
                {
                    tmpDirection = key.Value;
                    paddleState = PaddleState.Moving;


                }
           
              
            }
            Direction = tmpDirection;
        }

        private Vector2 ViewportCollision(GameTime dt)
        {
            // make a temporary position for collision check on the screen bounds
            Vector2 newPosition = position + ((Direction * (float)dt.ElapsedGameTime.TotalSeconds) * speed);

            if (newPosition.X < 0f || newPosition.X + width > this.viewrportBounds.Width) // check if the newposition is colliding with the viewport
            {
                buttonPress = false;
               return  position;    // if so, just return the position so that it wont move and not have a new position;
            }
            return newPosition;

        }

        


    }
}
