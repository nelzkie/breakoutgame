﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Shapes;

namespace Breaout_Final_Try.Gameplay.Entities
{
    public class Brick : GameEntity
    {

        public int Index { get; set; }
        public int points { get; set; }
        public bool Breakable { get; set; }
        public bool Hit { get; set; }
        public int powerUps { get; set; }
        public float scaleWidth { get; set; }
        public float scaleHeight { get; set; }
        public float showTimerAnimation { get; set; }
        public bool shown { get; set; }
        public GameTime gameTime;
        Vector2 scale;
        Texture2D image;
        Color color = Color.White;
        public int timeCounter { get; set; }
        public float timer { get; set; }
        public SpriteBatch batch { get; set; }
        SpriteFont font;

        
        public Brick(Game game, Texture2D image, SpriteBatch batch, Vector2 position, Color color, Vector2 scale, float speed, int index) : base(game, image, batch, position, scale, speed)
        {
            this.scale = scale;
            this.image = image;
            this.Index = index;
            this.color = color;
            shown = false;
            this.batch = batch;
            font = game.Content.Load<SpriteFont>("font");
            width = width / scale.X; // the width is already scaled from the gameEntity class so we just reversed the process to bring back the original size
            height /= scale.Y;  //// the Height is already scaled from the gameEntity class so we just reversed the process to bring back the original size

            height *=  scale.Y; // Scale the height base on the game leve. We dynamically scale the height base on how many rows we have in half of the screen height
            width *= scale.X;   // Same as this
           
           
      
        }

        public override void update(GameTime time)
        {
            bounds = new Rectangle((int)position.X, (int)position.Y, (int)width, (int)height);
            if (timeCounter > 0)
            {
               
                timeCounter--;
            }
         
            //base.update(time);
        }


        //public void render(SpriteBatch batch)
        //{
        //    batch.Draw(image, position, null, color, 0f, Vector2.Zero, new Vector2(1, scale), SpriteEffects.None, 0);
        //}
        public override void render(GameTime time)
        {
            
              
                //batch.DrawString(font,timeCounter.ToString(),position,Color.Red);
            batch.Draw(image, position, null, color, 0f, Vector2.Zero, scale, SpriteEffects.None, 0);
           // batch.DrawRectangle(bounds, Color.Red, 2f);
        }

   
    }
}
