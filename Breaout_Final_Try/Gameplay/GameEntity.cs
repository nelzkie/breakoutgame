﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Breaout_Final_Try.Gameplay.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Breaout_Final_Try.Gameplay
{
    public abstract class GameEntity 
    {
        SpriteBatch batch;
        Texture2D image;
        public Vector2 position { get; set; }
        public float scale { get; set; }
        public Vector2 Scale { get; set; }
        public Rectangle bounds { get; set; }
        public float width, height;
        public SpriteBatch Batch { get { return batch; } }
        Vector2 center;
        Game game;
        public float speed { get; set; }
        float rotation;
        public Color color { get; set; }
        Texture2D debugImage;
        public Rectangle viewrportBounds
        {
            get { return game.GraphicsDevice.Viewport.Bounds; }
        }

        public Vector2 Direction { get; set; }

        protected  GameEntity(Game game,Texture2D image, SpriteBatch batch, Vector2 position, float scale, float speed) 
        {
            this.batch = batch;
            this.image = image;
            this.position = position;
            color = Color.White;
          
            this.scale = scale;
            this.speed = speed;
            width = image.Width * scale;
            height = image.Height * scale;

            this.game = game;

            debugImage =  game.Content.Load<Texture2D>("dot");
            
            bounds = new Rectangle((int)position.X,(int)position.Y,(int)width,(int)height);
            center = new Vector2(image.Width / 2, image.Height / 2);
           
        }

        protected GameEntity(Game game, Texture2D image, SpriteBatch batch, Vector2 position, Vector2 scale, float speed)
        {
            this.batch = batch;
            this.image = image;
            this.position = position;


            this.Scale = scale;
         
            this.speed = speed;
            width = image.Width * scale.X;
            height = image.Height * scale.Y;

            this.game = game;

            debugImage = game.Content.Load<Texture2D>("dot");

            bounds = new Rectangle((int)position.X, (int)position.Y, (int)width, (int)height);
            center = new Vector2(image.Width / 2, image.Height / 2);

        }


        public virtual void update(GameTime time)
        {

           
            bounds = new Rectangle((int)position.X, (int)position.Y, (int)width, (int)height);
            if(this is PowerUps)
            {
               // Debug.WriteLine("powers " + scale + "   " + width);
            }

        }

        public  virtual void render(GameTime time)
        {
            batch.Draw(image,bounds,color);
            
            //batch.Draw(image, position, null,Color.White,0f,center, new Vector2(scale, scale), SpriteEffects.None, 0);

            //batch.Draw(image, position, null, Color.White, 0f, center, new Vector2(scale, scale), SpriteEffects.None, 0);
        }


    }
}
