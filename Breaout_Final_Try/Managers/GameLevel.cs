﻿using System;
using System.Collections.Generic;
using System.Deployment.Internal;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Breaout_Final_Try.Gameplay.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using OpenTK.Audio.OpenAL;

namespace Breaout_Final_Try.Managers
{
    public class GameLevel
    {
        string path;
        Game game;
        SpriteBatch batch;
        float scale;
        Texture2D texture;
        List<List<int>> tiledata = new List<List<int>>();
        public GameLevel(Game game,SpriteBatch batch,Texture2D texture,float scale)
        {
            this.game = game;
            this.batch = batch;
            this.scale = scale;
            this.texture = texture;
        }


        public void LoadLevel(string path,ref List<Brick> bricks, ref int levelbrickcounter)
        {
            
            string line = string.Empty;

            int counter = 0;


            if (File.Exists(path))
            {
                bricks.Clear();
              


                tiledata.Clear();
                using (StreamReader reader = new StreamReader(File.OpenRead(path)))
                {
                  
                    while ((line = reader.ReadLine()) != null)
                    {
                        List<int> row = new List<int>();

                        foreach (var c in line)
                        {
                            row.Add((int)char.GetNumericValue(c)); // add the value to our row list
                        }
                       
                        tiledata.Add(row); // this is a 2 dimensional list

                    }
                }
                    init(ref bricks,ref levelbrickcounter,Game1.SCREENWIDTH,Game1.SCREENHEIGHT /3 ); // will get 1/3 of the screen

            }
        }

        public void init(ref List<Brick> bricks,ref int brikcounter,int levelwidth, int levelHeight)
        {
            int points = 0;
            int height = tiledata.Count;
            int width = tiledata[0].Count;
            Color color = Color.White;
            bool breakable = false;
            int counter = 0;
            int brickCounter = 0;

            float unit_width = levelwidth / (float)width ;
            float unit_height =   levelHeight  / (float)height  ;
            float sizey = unit_height / 128;
            float sizex = unit_width / 128;
           
            for (int y = 0;y < height; y++)
            {
                
                for (int x = 0; x < width; x++)
                {
                   
                    counter++;
                    Vector2 pos = new Vector2(unit_width * x, unit_height * y);
                   
                    if(tiledata[y][x] == 1)
                    {
                        color = Color.Red;
                        breakable = false;
                    }
                    else
                    {
                        breakable = true;
                        brikcounter++;
                        color = Color.White;
                        if(tiledata[y][x] == 2)
                        {
                            points = 100;
                            color = new Color(0.2f, 0.6f, 1.0f);
                        }
                        if(tiledata[y][x] == 0)
                        {
                            points = 200;
                            color = new Color(0.8f, 0.8f, 0.4f);
                        }

                    }
                    brickCounter += 5;
                    Brick tmpBrick = new Brick(game, texture, batch, pos, color, new Vector2(sizex,sizey), 0f, counter);
                    tmpBrick.Breakable = breakable;
                    tmpBrick.points = points; // set the points for our score


                    tmpBrick.timeCounter = brickCounter;
                    bricks.Add(tmpBrick);
                }
            
                
            }
        }
    }
}
