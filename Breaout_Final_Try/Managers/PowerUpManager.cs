﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Breaout_Final_Try.Gameplay.Entities;
using Microsoft.Xna.Framework;

namespace Breaout_Final_Try.Managers
{
    public enum PowerUpType
    {
        Lengthen,
        Sticky
    }

    public class PowerUpManager : DrawableGameComponent
    {
        public Paddle paddle { get; set; }
        private float timer;
        private int timeCounter;

        Dictionary<PowerUpType, int> PowerUpDictionary;
        GameTime currentTime;

        public PowerUpManager(Game game, Paddle paddle) : base(game)
        {
            this.paddle = paddle;
            PowerUpDictionary = new Dictionary<PowerUpType, int>();
            PowerUpDictionary.Add(PowerUpType.Lengthen, 1);
            PowerUpDictionary.Add(PowerUpType.Sticky, 2);
        }

        public void Destroy(PowerUpType type)
        {
            PowerUpDictionary[type] = 0;
        }

        public bool PowerUpIsActive(PowerUpType type)
        {
            timer += (float)currentTime.ElapsedGameTime.TotalSeconds;
            timeCounter += (int)timer;
            if (timer >= 1.0F) timer = 0F;
            if(timeCounter >= 1)
            {
                PowerUpDictionary[type] -= timeCounter;
            }
            return PowerUpDictionary[type] > 0 ;
        }

        public override void Update(GameTime gameTime)
        {
            currentTime = gameTime;
            
          
            base.Update(gameTime);
        }


    }
}
