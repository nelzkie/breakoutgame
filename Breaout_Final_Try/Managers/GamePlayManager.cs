﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Breaout_Final_Try.Gameplay.Entities;
using Breaout_Final_Try.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Breaout_Final_Try.Gameplay
{
    public class GamePlayManager : DrawableGameComponent
    {

        SpriteBatch batch;
        SpriteFont font;
        Texture2D image;
        Texture2D texture;
        Game game;
        Paddle paddle;
        Ball ball;
        Rectangle viewportBounds;

        Effect effect;
        ParticleManager particleManager;
        FPSManager fpsManager;
        public Ball getBall { get { return ball; } }
        public Paddle getPaddle { get { return paddle; } }
        SpriteFont lifeFont;
        public GamePlayManager(Game game, SpriteBatch batch) : base(game)
        {
           
            
            this.batch = batch;
            this.game = game;
            viewportBounds = game.GraphicsDevice.Viewport.Bounds;
        
           

            LoadContent();

        }

        protected override void LoadContent()
        {
            float scale = 0.25f;
            float width, height;


            image = game.Content.Load<Texture2D>("paddle");
            width = image.Width * scale;
            height = image.Height * scale;
            paddle = new Paddle(game,image,batch,new Vector2(viewportBounds.Width / 2 - width / 2,viewportBounds.Height - height - 5f), scale:scale,speed: 400f);


            scale = 0.060f;
            image = game.Content.Load<Texture2D>("awesomeface");
            ball= new Ball(game,image,batch,Vector2.Zero, paddle,scale,speed:450f );

            particleManager = new ParticleManager(image,ball.position,batch);

            fpsManager = new FPSManager(game.Content.Load<SpriteFont>("font"));

            effect = game.Content.Load<Effect>("EffectsFX");

            lifeFont = game.Content.Load<SpriteFont>("lifeFont");

        }

        public override void Update(GameTime gameTime)
        {
         
            fpsManager.Update(gameTime);
            paddle.update(gameTime);
            ball.update(gameTime);
            //particleManager.EmitterLocation = new Vector2(ball.position.X , ball.position.Y);
            particleManager.EmitterLocation = new Vector2(ball.position.X + ball.width / 2, ball.position.Y + ball.height / 2);
            particleManager.Update((float)gameTime.ElapsedGameTime.TotalSeconds);
         

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {

           
            //effect.CurrentTechnique.Passes[0].Apply();
            paddle.render(gameTime);
            //effect.CurrentTechnique.Passes[0].Apply();
            particleManager.Draw(batch);
            fpsManager.Render(batch);
            ball.render(gameTime);

            batch.DrawString(lifeFont,ball.life.ToString(),new Vector2(viewportBounds.X,viewportBounds.Height - 25f),Color.GreenYellow );
           
            base.Draw(gameTime);
        }
    }
}
