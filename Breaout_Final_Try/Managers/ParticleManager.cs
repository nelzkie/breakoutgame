﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Breaout_Final_Try.Gameplay.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Breaout_Final_Try.Managers
{

    public class ParticleManager
    {
        private Random random;
        public List<Particle> particles1, particles2;    
        private Texture2D textures;
        private SpriteBatch batch;
        public Vector2 EmitterLocation { get; set; }

        public ParticleManager(Texture2D textures, Vector2 location, SpriteBatch batch)
        {
            EmitterLocation = location;
            this.textures = textures;
            particles1 = new List<Particle>();
            particles2 = new List<Particle>();
            random = new Random();

            this.batch = batch;
            
            for(int i = 0; i < 50; i++)
            {
                particles1.Add(GenerateNewParticle());
            }
        }

        public void Update(float time)
        {
            
            int total = 3;
            for (int i = 0; i < total; i++)
            {
                particles1.Add(GenerateNewParticle());


            }

            for (int particle = 0; particle < particles1.Count; particle++)
            {
                particles1[particle].Update(time);
                particles1[particle].Color = Color.Cyan * particles1[particle].TTL;
                if (particles1[particle].TTL <= 0)
                {
                    particles1.RemoveAt(particle);
                    particle--;
                }
            }
        }

        private void Generate(Particle particle )
        {
            
            Texture2D texture = textures;
            Vector2 position = EmitterLocation;
            Vector2 velocity = new Vector2(
                                    1f * (float)(random.NextDouble() * 2 - 1),
                                    1f * (float)(random.NextDouble() * 2 - 1));
            float angle = 0;
            float angularVelocity = 0.1f * (float)(random.NextDouble() * 2 - 1);
            Color color = new Color(
                        (float)random.NextDouble(),
                        (float)random.NextDouble(),
                        (float)random.NextDouble());
            float size = (float)random.NextDouble();
            float ttl = 1.0f;
            particle = new Particle(texture, position, velocity, angle, angularVelocity, color, size, ttl);
            particles1.Add(particle);

            
        }

        private Particle GenerateNewParticle()
        {
            Texture2D texture = textures;
            Vector2 position = EmitterLocation;
            Vector2 velocity = new Vector2(
                                    1f * (float)(random.NextDouble() * 2 - 1),
                                    1f * (float)(random.NextDouble() * 2 - 1));
            float angle = 0;
            float angularVelocity = 0.1f * (float)(random.NextDouble() * 2 - 1);
            Color color = new Color(
                        (float)random.NextDouble(),
                        (float)random.NextDouble(),
                        (float)random.NextDouble());
            float size = (float)random.NextDouble();
            float ttl = 1.0f;

            return new Particle(texture, position, velocity, angle, angularVelocity, color, size, ttl);
        }

        public void Draw(SpriteBatch spriteBatch)
        {

            

            for (int index = 0; index < particles1.Count; index++)
            {
                particles1[index].Draw(spriteBatch);
            }

        }
    }
}
