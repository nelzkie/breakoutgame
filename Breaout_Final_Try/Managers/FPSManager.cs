﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Breaout_Final_Try.Managers
{
    public class FPSManager
    {

        float timer;
        int timeCounter;
        public float FPS { get; set; }
        private SpriteFont font;




        public FPSManager(SpriteFont font)
        {
            this.font = font;
        }


        private void SHowLowFPS()
        {
            if(FPS < 59)
            {
                Debug.WriteLine("LOW FPS " + FPS);
            }
        }

        public void Update(GameTime time)
        {

            timer += (float)time.ElapsedGameTime.TotalSeconds;
            timeCounter += (int)timer;
            if (timer >= 1.0F) timer = 0F;
            FPS = 1 / (float)time.ElapsedGameTime.TotalSeconds;
            SHowLowFPS();
        }

        public void Render(SpriteBatch batch)
        {
            batch.DrawString(font, FPS.ToString(), new Vector2(10, 10), Color.Green);
        }
    }
}
