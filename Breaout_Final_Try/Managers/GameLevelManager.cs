﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Breaout_Final_Try.Gameplay;
using Breaout_Final_Try.Gameplay.Entities;
using Breaout_Final_Try.PopUp;
using Breaout_Final_Try.Screens;
using Breaout_Final_Try.Sound;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using MonoGame.Extended.Shapes;

namespace Breaout_Final_Try.Managers
{
    public enum Levels
    {
        level_1,
        level_2,
        level_3,
        level_4
    }

    public class GameLevelManager : DrawableGameComponent
    {
       
        #region  variable declaration
        public List<Brick> bricks = new List<Brick>();
        public List<PowerUps> PowerUpses = new List<PowerUps>();

        String path = String.Empty;
        Vector2 startPosition;
        Texture2D texture;
        float scale;
        Game game;
        Color color = Color.White;
        bool breakable = false;
        Matrix Scale; 
        float scalewidth, scaleheight;
        SpriteBatch batch;
        Camera2D camera;
        public Ball ball { get; set; }
        public Paddle paddle { get; set; }
        Effect effect;
        bool loadAnimation = true;
        float loadTimerAnimation;
        SpriteFont font;
        string level2 = @"Content/Level2.level";
        float timer;
        int timecounter;
        LevelPopUp levelPopUp;
        public bool completed { get; set; }

        int showBricks;
        int trialTry;
        float prevTimeCall,currentTimeCall;
        int brickCounter;
        int renderCounter;
        int randomCounter;
        int powerUpTTL;
        public Levels Levels = Levels.level_1;
        GameLevel gameLevel;
        private Random random = new Random();
        bool GameOver = false;
        SpriteFont gameOverFont;
        public int points { get; set; }
        String gOver = "Game Over";
        SoundHelper soundsHelper;
        Song song;
        #endregion

        public GameLevelManager(Game game,SpriteBatch   batch, Ball ball,Paddle paddle,float scale) : base(game)
        {
            this.game = game;
            this.scale = scale;
            this.batch = batch;
            //scalewidth = game.SWidth;
            //scaleheight = game.SHeight;

            Vector3 scalingfactor = new Vector3(scalewidth, scaleheight, 1);

             Scale = Matrix.CreateScale(scalingfactor);
            this.ball = ball;
            this.paddle = paddle;
            levelPopUp = new LevelPopUp(new Vector2(game.GraphicsDevice.Viewport.Width / 2, game.GraphicsDevice.Viewport.Height / 2),game.Content.Load<SpriteFont>("PopUpFont"),batch );
            soundsHelper = new SoundHelper(game.Content);
            gameOverFont = game.Content.Load<SpriteFont>("gameover");


            this.song = game.Content.Load<Song>("Ragna");
            MediaPlayer.Play(song);
            MediaPlayer.IsRepeating = true;
            MediaPlayer.MediaStateChanged += MediaPlayer_MediaStateChanged;

            setLevel(Levels);
        }

        void MediaPlayer_MediaStateChanged(object sender, System.
                                        EventArgs e)
        {
            // 0.0f is silent, 1.0f is full volume
            MediaPlayer.Volume -= 0.1f;
            MediaPlayer.Play(song);
        }

        private void setLevel(Levels type)
        {
            if(type == Levels.level_2)
            {
                levelPopUp.load("Level 2");
                loadContent(@"Content/Level2.level");
            }else if( type == Levels.level_3)
            {
                levelPopUp.load("Level 3");
                loadContent(@"Content/Level3.level");
            }else if(type == Levels.level_4)
            {
                levelPopUp.load("Level 4");
                loadContent(@"Content/Level4.level");
            }
            else
            {
                levelPopUp.load("Level 1");
                loadContent(@"Content/Level1.level");
            }
        }

        private void loadContent(String path)
        {
            texture = game.Content.Load<Texture2D>("block");
            effect = game.Content.Load<Effect>("EffectsFX");
             gameLevel= new GameLevel(game,batch,texture,scale);
            font = game.Content.Load<SpriteFont>("font");
            levelPopUp.doneAnimating = false; // trigger for the LevelPopUp class. Use as a trigger

            string line = string.Empty;

            int counter = 0;

           
        

            gameLevel.LoadLevel(path,ref bricks, ref levelBrickCounter);
        }

        private bool spawnChane(int chance)
        {
            Random random = new Random();
            int ch = random.Next() % chance;
            return ch == 0;
        }
        private void spawnPowerUp(Brick brick)
        {
            PowerUps powerUps;
            Texture2D texture;
            if (spawnChane(50))
            {
                texture = game.Content.Load<Texture2D>("powerup_increase");
                powerUps = new PowerUps(game, texture, batch, brick.position, paddle, brick.Scale, 150f, 1);
                powerUps.Activated = true;
                PowerUpses.Add(powerUps);
                return;


            }

            if (spawnChane(70))
            {
                texture = game.Content.Load<Texture2D>("powerup_sticky");
                powerUps = new PowerUps(game, texture, batch, brick.position, paddle, brick.Scale, 150f, 2);
                powerUps.Activated = true;
                PowerUpses.Add(powerUps);
                return;
            }


            if (spawnChane(90))
            {
                texture = game.Content.Load<Texture2D>("powerup_passthrough");
                powerUps = new PowerUps(game, texture, batch, brick.position, paddle, brick.Scale, 150f, 3);
                powerUps.Activated = true;
                PowerUpses.Add(powerUps);
               
            }

        }


        /// <summary>
        /// Handles the brick collision with the ball
        /// </summary>
        /// <param name="ball"></param>
        public void BrickCollisionChecker(Ball ball)
        {
            
            for(int i = 0; i < bricks.Count;i++)
            {
                if (ball.Circle_ABB_Collision(bricks[i]))
                {
                    if (!paddle.pass_through || !bricks[i].Breakable)
                    {
                        ball.deflectBall(bricks[i]);
                    }
                   
                    if (bricks[i].Breakable)
                    {
                        soundsHelper.Bounce().Play(); 
                        points += bricks[i].points;
                        bricks[i].Hit = true;
                        levelBrickCounter--;
                        spawnPowerUp(bricks[i]);
                        bricks.RemoveAt(i);
                       
                    }
                    else
                    {
                        Camera2D.shake = true;
                    }
                }
               
            }
        }

        float currentFrameTime, deltaTime, previousFrameTime;
        int levelBrickCounter; // counter for when it reach 0 move to next level
        public override void Update(GameTime time)
        {
            if (!GameOver)
            {
                currentFrameTime = (float)time.ElapsedGameTime.TotalSeconds;
                deltaTime = (currentFrameTime - previousFrameTime);




                // Load the next level
                // WE need the levelBrickcounter as it acts as atrigger when to load ther next level
                if (levelBrickCounter <= 0 || ball.life <= 0)
                {

                    Reset();

                }
                BrickCollisionChecker(ball); // check ball and brick collision


                // Power up and paddle collision, and assign a certain power up base on index
                for (int i = 0; i < PowerUpses.Count; i++)
                {
                    if (PowerUpses[i].PowerUpPaddleCollision(paddle))
                    {
                        if (PowerUpses[i].Index == 1)
                        {
                            paddle.pickPowerUp(Powers.lengthen, 10);

                        }
                        if (PowerUpses[i].Index == 2)
                        {
                            paddle.pickPowerUp(Powers.sticky, 10);

                        }
                        if (PowerUpses[i].Index == 3)
                        {
                            paddle.pickPowerUp(Powers.pass_through, 10);
                        }
                        PowerUpses.RemoveAt(i);
                    }
                    else
                    {
                        PowerUpses[i].update(time);

                    }
                }



                // Update each bricks
                for (int i = 0; i < bricks.Count; i++)
                {

                    bricks[i].update(time);
                }



                // if the animation is done
                if (!ball.levelAnimationDone)
                {
                    if (renderCounter >= bricks.Count)
                    {
                        ball.levelAnimationDone = true; // active the fire mechanism of ball
                    }
                }

                // This is the text that will pop up when we got into next level
                if (!levelPopUp.doneAnimating)
                {

                    levelPopUp.Update(time);
                }
            }

      
        }

        private void Reset()
        {
            ball.Reset_Ball();
            paddle.reset();
          
            PowerUpses.Clear();
            if(ball.life <= 0)
            {
                GameOver = true;
                MediaPlayer.Stop();
                soundsHelper.PlayBackgroundMusic().Stop(AudioStopOptions.AsAuthored);
                soundsHelper.GameOver().Play();


                // This trick will rewrite from the beginner of file
                http://stackoverflow.com/questions/12333892/how-to-write-to-beginning-of-file-with-stream-writer
                string path = Directory.GetCurrentDirectory() + "\\Score.txt";
                string str;
                using (StreamReader sreader = new StreamReader(path))
                {
                    str = sreader.ReadToEnd();
                }

                File.Delete(path);

                using (StreamWriter swriter = new StreamWriter(path, false))
                {
                    str = points + Environment.NewLine + str;
                    swriter.Write(str);
                }
          
                //ball.life = 3; // return to original life
                //ball.levelAnimationDone = false;


                //renderCounter = 0;
            }
            else
            {
                ball.levelAnimationDone = false;
                ball.life = 3; // return the original life for the next level
                Levels++;
            }
            setLevel(Levels);
            completed = true;
        }

        public override void Draw(GameTime gameTime)
        {
            
            effect.CurrentTechnique.Passes[2].Apply();

            if (!GameOver)
            {
                if (completed)
                {

                    levelPopUp.render(gameTime); // show the text for the next level
                }



                foreach (var powerUpse in PowerUpses)
                {
                    powerUpse.render(gameTime);
                }

            }
            else
            {
                Vector2 size = Vector2.Zero;
                size.X = gameOverFont.MeasureString(gOver).X;
        
                batch.DrawString(gameOverFont,"Game Over",new Vector2(game.GraphicsDevice.Viewport.Bounds.Width / 2 - size.X / 2, 
                    game.GraphicsDevice.Viewport.Bounds.Height / 2),Color.Yellow );
            }

            foreach (var brick in bricks)
            {

                if (!brick.shown)
                {
                 
                    if (brick.timeCounter <= 0)
                    {
                        
                        renderCounter++; // Keep track if all the bricks are rendered so we can activate the fire action of ball
                        brick.shown = true;
                    }
                }

              
                if (brick.shown)
                {
                    
                    brick.render(gameTime);
                }


            }
    
            base.Draw(gameTime);
        }

        
    }
}
