﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Breaout_Final_Try
{
    public abstract class GameScreen : DrawableGameComponent
    {
        List<GameComponent> gameComponents = new List<GameComponent>();

        public List<GameComponent> GameComponents
        {
            get { return gameComponents; }
            set { gameComponents = value; }
        }
        public GameScreen(Game game) : base(game)
        {
          
          
        }


        public override void Update(GameTime gameTime)
        {
           
            base.Update(gameTime);

            foreach (var gameComponent in gameComponents)
            {
                gameComponent.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            foreach (var gameComponent in gameComponents)
            {
                if(gameComponent is DrawableGameComponent)
                {
                  ((DrawableGameComponent)gameComponent).Draw(gameTime);  
                }
            }
        }
    }
}
