﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

namespace Breaout_Final_Try.Sound
{
    public class SoundHelper
    {
        private AudioEngine audioEngine;

        private SoundBank soundBank;
        private WaveBank waveBank;

        public SoundHelper(ContentManager content)
        {
            audioEngine = new AudioEngine(content.RootDirectory + "/Breakout.xgs");
            waveBank = new WaveBank(audioEngine, content.RootDirectory + "/WB.xwb");
            soundBank = new SoundBank(audioEngine, content.RootDirectory + "/sb.xsb");
        }

        public Cue PlayBackgroundMusic()
        {
            Cue cue = soundBank.GetCue("skyrim");

            return cue;
        }

        public Cue Bounce()
        {
            Cue cue = soundBank.GetCue("bounce");

            return cue;
        }

        public Cue GameOver()
        {
            Cue cue = soundBank.GetCue("gameover");
            return cue;
        }

        public void PlayExplosion()
        {
            Cue cue = soundBank.GetCue("bomb");
            cue.Play();
        }
    }
}
