﻿using System.Diagnostics;
using Breaout_Final_Try.Gameplay;
using Breaout_Final_Try.Gameplay.Entities;
using Breaout_Final_Try.Screens;
using Breaout_Final_Try.Sound;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Breaout_Final_Try
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public static GameScreen screen;
        MenuScreen menuScreen;
        public  GamePlayScreen gamePlayScreen;
        KeyboardState oldState, currentState;
        Ball ball;

        float screenwidth, screenheight;

        public float SWidth { get { return screenwidth; } }
        public float SHeight { get { return screenheight; } }
        Camera2D camera = new Camera2D();
        public Ball GetBall { get { return ball; } }
        public static int SCREENWIDTH = 1024, SCREENHEIGHT = 768;
        public SoundHelper soundsHelper;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = SCREENWIDTH;
            graphics.PreferredBackBufferHeight = SCREENHEIGHT;

            //screenwidth = graphics.GraphicsDevice.PresentationParameters.BackBufferWidth / 1020;
            //screenheight = graphics.GraphicsDevice.PresentationParameters.BackBufferHeight/760;
            Content.RootDirectory = "Content";
            soundsHelper = new SoundHelper(Content);
            //soundsHelper.PlayBackgroundMusic().Play();
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            menuScreen = new MenuScreen(this,spriteBatch,Content.Load<SpriteFont >("font"),Content.Load<Texture2D>("MenuBG"));
            Components.Add(menuScreen);

            gamePlayScreen = new GamePlayScreen(this,spriteBatch);
            //Components.Add(gamePlayScreen);

            Debug.WriteLine(Components.Count);
            screen = menuScreen;
            base.LoadContent();

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            currentState = Keyboard.GetState();
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
           
            //NOTE we gotta use better event call than this
            for(int i=  0; i < Components.Count; i++)
            {
                if (screen is MenuScreen)
                {
                   
                    if (CheckPressOnce(Keys.Enter))
                    {
                      //soundsHelper.PlayBackgroundMusic().Stop(AudioStopOptions.AsAuthored);
                        if (menuScreen.SelectedIndex == 0)
                        {
                            screen = gamePlayScreen;
                            Components.Add(gamePlayScreen);
                            
                            Components.RemoveAt(i);


                        }
                        if(menuScreen.SelectedIndex == 1)
                        {
                            Components.Add(new GameOver(this,spriteBatch));
                            Components.RemoveAt(i);
                        }
                        if(menuScreen.SelectedIndex == 2)
                        {
                            Exit(); 
                        }
                    }
                }
            }
            camera.Update(gameTime);
            base.Update(gameTime);
            oldState = currentState;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin( blendState: null, samplerState: null, depthStencilState: null, rasterizerState: null, effect: null, transformMatrix: camera.get_transformation(GraphicsDevice));
            //spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            base.Draw(gameTime);
            spriteBatch.End();
        }
        private bool CheckPressOnce(Keys key)
        {
            return currentState.IsKeyUp(key) && oldState.IsKeyDown(key);
        }
    }
}
