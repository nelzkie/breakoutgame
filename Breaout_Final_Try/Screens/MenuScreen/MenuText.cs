﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Breaout_Final_Try
{
    public class MenuText : DrawableGameComponent
    {
        SpriteBatch batch;
        SpriteFont font;
        float width, height;
        string[] items;
        Vector2 position;
        int selectedIndex;
        KeyboardState oldStae, currentState;
        Color fontColor = Color.White;

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                selectedIndex = value;
                if (selectedIndex < 0)
                    selectedIndex = 0;
                if (selectedIndex > items.Length)
                    selectedIndex = items.Length - 1;
            }
        }

        public MenuText(Game game, SpriteBatch batch, SpriteFont font) : base(game)
        {
          
            this.batch = batch;
            this.font = font;
            items = new[] { "Start Game", "High Score", " End Game"};
            load();
        }

        private void load()
        {
            foreach (var item in items)
            {
                Vector2 size = font.MeasureString(item);
                width = size.X;
                height += size.Y; // get the total number of height for positioning
                
            }

            position = new Vector2(Game.GraphicsDevice.Viewport.Width / 2 - width / 2,
                Game.GraphicsDevice.Viewport.Height / 2 - height / 2);
        }


        public override void Update(GameTime gameTime)
        {
            currentState = Keyboard.GetState();

            Selection(); // for the selection of menus using Up and Down Keys

            base.Update(gameTime);

            oldStae = currentState;
        }


        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            Vector2 tmpPosition = position;
            for(int i=  0; i < items.Length; i++)
            {
                if(i == selectedIndex)
                {
                    fontColor = Color.DarkKhaki;
                }
                else
                {
                    fontColor = Color.BlueViolet;
                }

                batch.DrawString(font,items[i], tmpPosition, fontColor);
                tmpPosition.Y += font.LineSpacing; // move the text on the bottom after the first text
            }
        }

        private bool CheckPressOnce(Keys key)
        {
            return currentState.IsKeyUp(key) && oldStae.IsKeyDown(key);
        }

        private void Selection()
        {
            if (CheckPressOnce(Keys.Down))
            {
                selectedIndex++;
                if (selectedIndex == items.Length)
                    selectedIndex = 0; // return to the first option
            }

            if (CheckPressOnce(Keys.Up))
            {
                selectedIndex--;
                if (selectedIndex < 0)
                    selectedIndex = items.Length - 1; // return to the last option
            }
        }
    }
}
