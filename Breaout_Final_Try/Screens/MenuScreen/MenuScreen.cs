﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Breaout_Final_Try
{
    class MenuScreen : GameScreen
    {
        Texture2D image;
        SpriteBatch batch;
        SpriteFont font;
        MenuText menuText;

        public int SelectedIndex
        {
            get { return menuText.SelectedIndex; }
            set { menuText.SelectedIndex = value; }
        }
        public MenuScreen(Game game, SpriteBatch batch,SpriteFont font, Texture2D  image) : base(game)
        {
            this.image = image;
            this.font = font;
            this.batch = batch;

            menuText = new MenuText(game,batch,font);
            GameComponents.Add(menuText);
        }

      

        public override void Draw(GameTime gameTime)
        {

            // We use rectangle instead of position vector. This will automatically resize the image according to screen size
            batch.Draw(image, GraphicsDevice.Viewport.Bounds, Color.White); 
            base.Draw(gameTime);
        }
    }
}
