﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Breaout_Final_Try.Screens
{
    public class GameOver : GameScreen
    {
        Texture2D texture;
        SpriteBatch batch;
        public int points { get; set; }
        SpriteFont scorefont;
        List<String> listPoints = new List<string>();
        Vector2 size;
        Vector2 position;
        int viewCounter;
        public GameOver(Game game, SpriteBatch batch) : base(game)
        {
            texture = game.Content.Load<Texture2D>("MenuBG");
            scorefont = game.Content.Load<SpriteFont>("gameover");
            this.batch = batch;
            position = new Vector2(game.GraphicsDevice.Viewport.Width / 2, game.GraphicsDevice.Viewport.Height / 2);
            // Read in lines from file.
            foreach (string line in File.ReadLines("Score.txt"))
            {
                viewCounter++;
                if (viewCounter <= 10)
                {
                    listPoints.Add(line);
                }
          
            }

            foreach (var listPoint in listPoints)
            {
                size.X = scorefont.MeasureString(listPoint.ToString()).X;
                size.Y += scorefont.MeasureString(listPoint.ToString()).Y + scorefont.LineSpacing;
            }
        }

        public override void Draw(GameTime gameTime)
        {

            batch.Draw(texture,Vector2.Zero,Color.White);
            Vector2 tmpPosition = position;
            foreach (var points in listPoints)
            {
                
              // tmpPosition.X = scorefont.MeasureString(points.ToString()).X;
                tmpPosition.Y += scorefont.LineSpacing;
         
                batch.DrawString(scorefont,points.ToString() + " score points",new Vector2( size.X / 2, tmpPosition.Y - size.Y / 2), Color.BurlyWood);

            }
            
            if(listPoints.Count <= 0)
            {
                batch.DrawString(scorefont, 0 + " score points", new Vector2(size.X / 2, tmpPosition.Y - size.Y), Color.BurlyWood);
            }

            base.Draw(gameTime);
        }
    }
}
