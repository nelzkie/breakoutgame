﻿sampler s0;
float4 ColorTexture;
float4 PixelShaderFunction(float2 coords: TEXCOORD0) : COLOR0
{
	float4 color = tex2D(s0, coords);
	color.rgb = color.gbr;
	return color;
}
float4 PixelShaderFunction2(float2 coords: TEXCOORD0) : COLOR0
{
	float4 color = tex2D(s0, coords);
	color = ColorTexture * color;

	return color;
}
float4 PixelShaderFunction3(float2 coords: TEXCOORD0) : COLOR0
{
	float4 color = tex2D(s0, coords);
	color.rgb = 1 - color.rgb;
	return color;
}

technique Technique1
{
	pass Pass1
	{
		PixelShader = compile ps_2_0 PixelShaderFunction();
	}
	pass Pass2
	{
		PixelShader = compile ps_2_0 PixelShaderFunction2();
	}
	pass Pass3
	{
		PixelShader = compile ps_2_0 PixelShaderFunction3();
	}

}