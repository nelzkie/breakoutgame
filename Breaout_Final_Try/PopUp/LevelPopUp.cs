﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Breaout_Final_Try.PopUp
{
    public class LevelPopUp : PopUpManager
    {
        public bool shown;
        float fadeOut = 1.0f;
        public bool doneAnimating = false;
        public LevelPopUp(Vector2 position, SpriteFont font, SpriteBatch batch) : base(position, font, batch)
        {

        }

        public override void Update(GameTime gameTime)
        {
            if (shown)
            {
                fadeOut -= (float)gameTime.ElapsedGameTime.TotalSeconds * 0.01f;
                color *= fadeOut;
                if(fadeOut < 0.5f)
                {
                    doneAnimating = true;
                    shown = false;
                }
                
            }
            base.Update(gameTime);
        }

        public override void render(GameTime gameTime)
        {

            if (!shown)
            {
                
                shown = true;
            }

            batch.DrawString(font, message, position, color);
            base.render(gameTime);
        }
    }
}
