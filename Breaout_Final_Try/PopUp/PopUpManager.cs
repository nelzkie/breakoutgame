﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Breaout_Final_Try.PopUp
{
    public class PopUpManager
    {
        protected Vector2 position;
        protected SpriteFont font;
        protected SpriteBatch batch;
        protected String message;
        public Color color { get; set; }
        public PopUpManager(Vector2 position, SpriteFont font, SpriteBatch batch)
        {
            this.position = position;
            this.font = font;
            this.batch = batch;

            color = Color.White;
        }
        public void load(String msg)
        {
            message = msg;
        }

        public virtual void Update(GameTime gameTime)
        {
            
        }

        public virtual void render(GameTime gameTime)
        {
            
        }

    }
}
